% This function plots x y traces from processed data, and allows tagging
% and tossing trials for each individual type of em
%

%%%%%%%%%%% INPUT:
% pptrials: structure of processed data including trial information and
% data
%
%%%%%%%%%%% Properties: Optional properties
%
% 'StartTime': string of variable of period of
% interest (which part of the trace are you interest in?

% 'EndTime': string of variable of period of
% interest (which part of the trace are you interest in?

% 'AxisWindow': size of viewing window (one positive value = -val to +val).

% 'Folder': select filepath you want your new clean data to save.

% 'NewFileName' = what .mat file name do you want it saved as?

% 'Conversion' = 1; %machine sampling rate conversion factor (ie DPI = 1, dDPI = 1000/330)

% 'WhichTrials = if you only want subset of trials to be plotted.
%
%%%%%%%%%%%%% OUTPUT:
% pptrials = new clean data

% HISTORY:
% This function is based on the pptrials generated from the basicEIS
% function

% 2020 @APLAB Ashley Clark



function pptrials = cleaningTrialsVisually(pptrials, varargin)

poiStartName = 'TimeTargetON';
poiEndName = 'TimeTargetOFF';
axisWindow = 60;
filepath = pwd;
newFileName = 'pptrials.mat';
conversionFactor = 1000/340; %machine sampling rate conversion factor (ie DPI = 1, dDPI = 1000/330)
trialId = 1:length(pptrials);

k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'StartTime'
            poiStartName =  Properties{k + 1};
            Properties(k:k+1) = [];
        case 'EndTime'
            poiEndName = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'AxisWindow'
            axisWindow = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Folder'
            filepath = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'NewFileName'
            newFileName = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Conversion'
            conversionFactor = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'WhichTrials'
            trialId = Properties{k + 1};
            Properties(k:k+1) = [];
        otherwise
            k = k + 1;
    end
end

nameFile = sprintf('%s',newFileName);
PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
   
    trialCounter = 1;
    for driftIdx = 1:inf
         
        if (trialCounter) > length(trialId)
            return;
        end
        figure('position',[800, 100, 1500, 800])%changed by SJ 2/9/21 bc it was off the screen 
        %figure('position',[2300, 100, 1500, 800])
        currentTrialId = trialId(trialCounter);
        hold off
        
%         poiStart = round(pptrials{currentTrialId}.(poiStartName));
%         poiEnd = double(min(pptrials{currentTrialId}.(poiEndName)));
        
%         if pptrials{currentTrialId}.(poiEndName) <= 0
%             return;
%         end
        
%         poi = fill([poiStart, poiStart ...
%             poiEnd, poiEnd], ...
%             [-50, 50, 50, -50], ...
%             'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
%         
%         xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
%         yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pxAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pxAngle;
        
        hold on
        hx = plot(1:conversionFactor:length(xTrace)*conversionFactor,xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
        hy = plot(1:conversionFactor:length(yTrace)*conversionFactor,yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
        axis([0 length(xTrace)*conversionFactor -axisWindow axisWindow])
        
%         axis([poiStart - 400, poiEnd + 400, -(axisWindow), axisWindow])
%         axis([0 length(hx) -(axisWindow), axisWindow])
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i', currentTrialId)); %
        
        set(gca, 'FontSize', 12)
        
        poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0], conversionFactor); %red
        poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0], conversionFactor); %red - will still plot, just not saved seperate from MS
        poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0], conversionFactor); %green
        poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0], conversionFactor); %black
        poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1], conversionFactor); %blue
        poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1], conversionFactor); %pink
        
        
        if any(string(fieldnames(pptrials{currentTrialId})) == 'spikes')
            poiTest1 = plotColorsOnTraces(pptrials, currentTrialId, 'spikes', [.749 .019 1], conversionFactor); %pink
        end
        if any(string(fieldnames(pptrials{currentTrialId})) == 'oscillating_noise')
            poiTest2 = plotColorsOnTraces(pptrials, currentTrialId, 'oscillating_noise', [0 0 1], conversionFactor); %blue
        end
        
        legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
            {'X','Y','MS/S','D','NoTrack','Invalid','Blink'},'FontWeight','bold') %%%Ignores Saccades
        
        contType = input...
            ('TAG a spikes(1), oscillating_noise(2), drift(3), \n No Track(4), Invalid(5), Blinks(6), OR \n Back a trial(7), Stop(0), AutoPrune(8), Clear All (9), \n Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Spike(a) or Change Spike Labelled(b)?','s');
            em = 'spikes'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a oscillating_noise(a) or Change oscillating_noise Labelled(b)?','s');
            em = 'oscillating_noise'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts';
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 75);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10);
            continue;
        elseif contType == 9
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'invalid', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 10000);
            continue;
        else
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(cd, newFileName),'pptrials')
            break;
        elseif cont == 'a'
            if strcmp(em, 'spikes') || strcmp (em, 'oscillating_noise')
                if any(string(fieldnames(pptrials{currentTrialId})) == em)
                    numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;
                else
                    numTaggedInTrace = 1;
                end
            else
                numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;
            end
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
            
            pptrials{currentTrialId}.(em).start(numTaggedInTrace) = round(startTime/conversionFactor);
            pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = round((duration/conversionFactor) - round(startTime/conversionFactor));
            
            fprintf('Saving pptrials\n')
            save(fullfile(cd, newFileName),'pptrials')
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId}.(em).start, 3)*(1000/330))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(1000/330)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(1000/330));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(1000/330))) - round(newStartTime(numW)/(1000/330));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(cd, newFileName),'pptrials')
            continue;
        elseif cont == 'z'
            if trialCounter == 1
                trialCounter = 1;
            else
                trialCounter = trialCounter-1;
            end
            continue;
        end
        save(fullfile(cd, newFileName),'pptrials')
        trialCounter = trialCounter + 1;
    end
end

close

function [ poiEM ] = plotColorsOnTraces( pptrials, currentTrialId, em, c, convert )
%Plots the color and location of the wanted EM in the gui
poiEM = [];

for i = 1:length(pptrials{currentTrialId}.(em).start)
    startTime = pptrials{currentTrialId}.(em).start(i) * convert;
    if isempty(pptrials{currentTrialId}.(em).duration)
        durationTime = 5;
    else
        durationTime = pptrials{currentTrialId}.(em).duration(i) * convert;
    end
    poiEM = fill([startTime, startTime ...
        startTime + durationTime, ...
        startTime + durationTime], ...
        [-35, 35, 35, -35], ...
        c, 'EdgeColor', c, 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
end

function [ pptrials ] = autoPrune( pptrials, currentTrialId, em, threshold )
%Automatically prunes tags that are too short to be real
%   em = the type of eye movements (ie drifts, saccades, notracks...)
%   threshold = the minimum number of samples that are required for it
%   to be real

for ii = 1:length(pptrials{currentTrialId}.(em).duration)
    if pptrials{currentTrialId}.(em).duration(ii) < threshold
        pptrials{currentTrialId}.(em).start(ii) = 0;
        pptrials{currentTrialId}.(em).duration(ii) = 0;
    end
end